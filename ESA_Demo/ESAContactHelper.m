//
//  ESAContactHelper.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAContactHelper.h"
#import "ESAContact.h"

static NSString *const kESAContactsListKey = @"ESAContactsList";

@implementation ESAContactHelper
#pragma mark - Helper
//持久化联系人数据到本地方法
+ (void)saveContacts:(NSArray *)contactArray {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dataArray = [NSMutableArray new];
    for (ESAContact *contact in contactArray) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:contact];
        [dataArray addObject:data];
    }
    NSArray *array = [NSArray arrayWithArray:dataArray];
    [userDefaults setObject:array forKey:kESAContactsListKey];
    [userDefaults synchronize];
}

//从本地读取联系人数据方法
+ (NSMutableArray *)readContacts {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *mutableArray = [NSMutableArray new];
    NSArray *dataArray = [userDefaults objectForKey:kESAContactsListKey];
    if (dataArray) {
        for (NSData *data in dataArray) {
            ESAContact *contact = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [mutableArray addObject:contact];
        }
    }
    return mutableArray;
}
@end
