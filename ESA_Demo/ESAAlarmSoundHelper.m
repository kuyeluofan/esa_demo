//
//  ESAAlarmSoundHelper.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAAlarmSoundHelper.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation ESAAlarmSoundHelper

static void completionCallback(SystemSoundID mySSID)
{
    // Play again after sound play completion
    AudioServicesPlaySystemSound(mySSID);
}

SystemSoundID coughVoice;

+ (void)alarmWithVibrate:(BOOL)needVibrate {
    NSURL *voiceURL = [[NSBundle mainBundle]URLForResource:@"alert" withExtension:@"wav"];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(voiceURL),&coughVoice);
//    AudioServicesAddSystemSoundCompletion(coughVoice,NULL,NULL,(void*)completionCallback,NULL);
    AudioServicesPlaySystemSound(coughVoice);
    if (needVibrate) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate); // 控制手机振动
    }
    
}

@end
