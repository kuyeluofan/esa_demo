//
//  ViewController.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAMainViewController.h"
#import "Masonry.h"
#import "ESAStartViewController.h"
#import "ESASettingsViewController.h"

@interface ESAMainViewController ()

@property (strong, nonatomic) UIImageView *logoImageView;
@property (strong, nonatomic) UIButton *settingsButton;
@property (strong, nonatomic) UIImageView *settingsImageView;
@property (strong, nonatomic) UIButton *startButton;
@property (strong, nonatomic) UIImageView *startImageView;
@property (strong, nonatomic) UIImageView *watercolorImageView;


@end

@implementation ESAMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.logoImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo_ESA"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView;
    });

    self.settingsButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"Icon_Settings"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(gotoSettingsVC) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    self.settingsImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Label_Settings"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView;
    });
    
    self.startButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"Icon_Start"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(gotoStartVC) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    self.startImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Label_Start"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView;
    });
    
    self.watercolorImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bg_Watercolor"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView;
    });
    
    [self.view addSubview:self.watercolorImageView];
    [self.view addSubview:self.logoImageView];
    [self.view addSubview:self.settingsButton];
    [self.view addSubview:self.settingsImageView];
    [self.view addSubview:self.startButton];
    [self.view addSubview:self.startImageView];
    
    
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view).offset(12);
        make.top.equalTo(self.view).offset(MASAttributeTop);
        make.height.width.equalTo(@240);
    }];
    
    [self.settingsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX).offset(-80);
        make.top.equalTo(self.logoImageView.mas_bottom).offset(-10);
        make.height.width.equalTo(@80);
    }];
    
    [self.settingsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.settingsButton);
        make.top.equalTo(self.settingsButton.mas_bottom).offset(-10);
        make.height.width.equalTo(@120);
    }];
    
    [self.startButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX).offset(80);
        make.centerY.equalTo(self.settingsButton);
        make.height.width.equalTo(@90);
    }];
    
    [self.startImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.startButton);
        make.centerY.equalTo(self.settingsImageView).offset(-5);
        make.height.width.equalTo(@90);
    }];
    
    [self.watercolorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@600);
        make.height.equalTo(@400);
        make.bottom.equalTo(self.view).offset(80);
    }];
    
}

#pragma mark - Event Response
- (void)gotoSettingsVC {
    ESASettingsViewController *settingsVC = [ESASettingsViewController new];
    [self.navigationController showViewController:settingsVC sender:self];
}


- (void)gotoStartVC {
    ESAStartViewController *startVC = [ESAStartViewController new];
    [self.navigationController showViewController:startVC sender:self];
}


@end
