//
//  ESASettingsViewController.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESASettingsViewController.h"
#import "Masonry.h"
#import "ESAContactsListViewController.h"
#import "ESAAlarmSoundHelper.h"

static NSString *const kESAAlarmSwitchStatusKey = @"ESAAlarmSwitchStatusKey";
@interface ESASettingsViewController ()

@property (strong, nonatomic) UIImageView *bgImageView;
@property (strong, nonatomic) UIButton *contactIconButton;
@property (strong, nonatomic) UIButton *contactTextButton;
@property (strong, nonatomic) UIButton *alarmIconButton;
@property (strong, nonatomic) UIButton *alarmTextButton;
@property (strong, nonatomic) UISwitch *alarmSwitch;
@property (strong, nonatomic) UIButton *moreInfoButton;

@end

@implementation ESASettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.clipsToBounds = YES;
    self.navigationItem.title = @"Settings";
    
    self.bgImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bg_Settings"]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView;
    });

    
    self.contactIconButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [button setImage:[UIImage imageNamed:@"Icon_Contact"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onCickContactButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    
    self.contactTextButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [button setImage:[UIImage imageNamed:@"Label_Contact"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onCickContactButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    
    self.alarmIconButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [button setImage:[UIImage imageNamed:@"Icon_Alarm"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onClickAlarmButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    
    self.alarmTextButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [button setImage:[UIImage imageNamed:@"Label_Alarm"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onClickAlarmButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    self.alarmSwitch = ({
        UISwitch *alarmSwitch = [UISwitch new];
        [alarmSwitch setOn:[self readAlarmSwitchStatus]];
        [alarmSwitch addTarget:self action:@selector(saveAlarmSwitchStatus) forControlEvents:UIControlEventTouchUpInside];
        alarmSwitch;
    });
    
    self.moreInfoButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [[button imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [button setImage:[UIImage imageNamed:@"Icon_MoreInfo"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onClickMoreInfo) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    
    [self.view addSubview:self.bgImageView];
    [self.view addSubview:self.contactIconButton];
    [self.view addSubview:self.contactTextButton];
    [self.view addSubview:self.alarmIconButton];
    [self.view addSubview:self.alarmTextButton];
    [self.view addSubview:self.alarmSwitch];
    [self.view addSubview:self.moreInfoButton];
    
    
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.contactIconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view).offset(-100);
        make.centerY.equalTo(self.view).offset(-100);
        make.height.width.equalTo(@100);
    }];
    
    [self.contactTextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contactIconButton.mas_right).offset(6);
        make.centerY.equalTo(self.contactIconButton);
        make.height.equalTo(@100);
        make.width.equalTo(@200);
    }];
    
    [self.alarmIconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contactIconButton);
        make.centerY.equalTo(self.view).offset(60);
        make.height.width.equalTo(@70);
    }];
    
    [self.alarmTextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.alarmIconButton.mas_right).offset(24);
        make.centerY.equalTo(self.alarmIconButton);
        make.height.equalTo(@100);
        make.width.equalTo(@150);
    }];
    
    [self.moreInfoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.equalTo(@60);
        make.width.equalTo(@240);
        make.bottom.equalTo(self.view.mas_bottom).offset(-40);
    }];
    
    [self.alarmSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.alarmTextButton);
        make.left.equalTo(self.alarmTextButton.mas_right).offset(-8);
    }];
    
}


#pragma mark - Event Response
- (void)onCickContactButton {
    ESAContactsListViewController *contactListVC = [ESAContactsListViewController new];
    [self.navigationController showViewController:contactListVC sender:self];
}

- (void)onClickAlarmButton {
    //预留处理点击Alarm的事件
    // [ESAAlarmSoundHelper alarmWithVibrate:YES];
}

- (void)onClickMoreInfo {
    //预留处理点击MoreInfo的事件
}

#pragma mark - Helper
//将开关状态持久化到本地
- (void)saveAlarmSwitchStatus {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:self.alarmSwitch.isOn forKey:kESAAlarmSwitchStatusKey];
    if (self.alarmSwitch.isOn) {
        [ESAAlarmSoundHelper alarmWithVibrate:YES];
    }
    [userDefaults synchronize];
}

//读取本地存储的开关状态
- (BOOL)readAlarmSwitchStatus {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:kESAAlarmSwitchStatusKey];
}

@end
