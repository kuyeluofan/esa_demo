//
//  ESAAlarmSoundHelper.h
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESAAlarmSoundHelper : NSObject

+ (void)alarmWithVibrate:(BOOL)needVibrate;

@end
