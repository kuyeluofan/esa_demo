//
//  ESAContact.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAContact.h"

@implementation ESAContact
+ (instancetype)contactWithName:(NSString *)name number:(NSString *)number {
    return [[ESAContact alloc] initWithName:name number:number];
}

- (instancetype)initWithName:(NSString *)name number:(NSString *)number {
    self = [super init];
    if (self) {
        _name = name;
        _number = number;
    }
    return self;
}

//持久化自定义Model需要实现的协议
#pragma mark - NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"ESAContactName"];
    [aCoder encodeObject:_number forKey:@"ESAContactNumber"];
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectForKey:@"ESAContactName"];
        self.number = [aDecoder decodeObjectForKey:@"ESAContactNumber"];
    }
    return self;
}

//实现isEqual方法以便对两个ESAContact进行对比,如果name相同，即认为是同一contact
- (BOOL)isEqual:(id)object {
    if (object == self) {
        return YES;
    }
    
    if (!object || ![object isKindOfClass:[self class]]) {
        return NO;
    }
    ESAContact *ESAObject = (ESAContact *)object;
    
    if ([ESAObject.name isEqual:self.name]) {
        return YES;
    } else {
        return NO;
    }
}


@end
