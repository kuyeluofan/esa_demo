//
//  ESAContactHelper.h
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESAContactHelper : NSObject

+ (void)saveContacts:(NSArray *)contactArray;
+ (NSMutableArray *)readContacts;

@end
