//
//  ESAStartViewController.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAStartViewController.h"
#import "Masonry.h"
#import <MessageUI/MessageUI.h>
#import "ESAContact.h"
#import "ESAContactHelper.h"
#import "ESAAlarmSoundHelper.h"

@interface ESAStartViewController ()<MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) UIImageView *bgImageView;
@property (strong, nonatomic) UIButton *messageButton;
@property (strong, nonatomic) UIButton *callButton;
@property (strong, nonatomic) NSTimer *t;

@end

@implementation ESAStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    self.t = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector:@selector(alarmWithTimeInterval)
                                                userInfo:nil
                                                 repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.t forMode:NSRunLoopCommonModes];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self.t invalidate];
    self.t = nil;
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.clipsToBounds = YES;
    self.navigationItem.title = @"Start";
    
    self.bgImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Bg_Start"]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView;
    });
    
    self.messageButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"Icon_Message"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onClickMessageButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    self.callButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"Icon_Call"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        [button addTarget:self action:@selector(onClickCallButton) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    [self.view addSubview:self.bgImageView];
    [self.view addSubview:self.messageButton];
    [self.view addSubview:self.callButton];
    
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.messageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view).offset(-100);
        make.height.width.equalTo(@100);
    }];
    
    [self.callButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view).offset(100);
        make.height.width.equalTo(@100);
    }];
}

- (void)alarmWithTimeInterval{
    [ESAAlarmSoundHelper alarmWithVibrate:YES];
}

#pragma mark - Event Response
- (void)onClickMessageButton {
    //预留处理点击Message的事件
    __block NSUInteger secondNumber = 5;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Send message in %lu sec", (unsigned long)secondNumber] preferredStyle:UIAlertControllerStyleAlert];
    
    __block NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (secondNumber > 1) {
            secondNumber--;
            alertController.message = [NSString stringWithFormat:@"Send message in %lu sec", (unsigned long)secondNumber];
            
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
            [timer invalidate];
            timer = nil;
            [self.t invalidate];
            self.t = nil;
        }

    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Send Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //立即发送事件
        [timer invalidate];
        timer = nil;
        [self.t invalidate];
        self.t = nil;
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //取消事件
        [timer invalidate];
        timer = nil;
        [self.t invalidate];
        self.t = nil;
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        NSRunLoop *runloop=[NSRunLoop currentRunLoop];
        [runloop addTimer:timer forMode:NSDefaultRunLoopMode];
    }];
    

//    [self sendMessageTo:[ESAContactHelper readContacts] body:@"Test!!!"];
}

- (void)onClickCallButton {
    //预留处理点击Call的事件
    __block NSUInteger secondNumber = 5;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Call for you via VoIP in %lu sec", (unsigned long)secondNumber] preferredStyle:UIAlertControllerStyleAlert];
    
    __block NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (secondNumber > 1) {
            secondNumber--;
            alertController.message = [NSString stringWithFormat:@"Call for you via VoIP in %lu sec", (unsigned long)secondNumber];
            
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
            [timer invalidate];
            timer = nil;
            [self.t invalidate];
            self.t = nil;
        }
        
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Call Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //立即发送事件
        [timer invalidate];
        timer = nil;
        [self.t invalidate];
        self.t = nil;
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //取消事件
        [timer invalidate];
        timer = nil;
        [self.t invalidate];
        self.t = nil;
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        NSRunLoop *runloop=[NSRunLoop currentRunLoop];
        [runloop addTimer:timer forMode:NSDefaultRunLoopMode];
    }];
    

}

#pragma mark - Message Event & Delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:
            //信息已发送
            break;
        case MessageComposeResultFailed:
            //信息发送失败
            break;
        case MessageComposeResultCancelled:
            //信息发送被取消
            break;
        default:
            break;
    }
}

- (void)sendMessageTo:(NSArray *)contacts body:(NSString *)content {
    if (!contacts) {
        //没有收件人信息
        return;
    }
    
    if (!content) {
        //短信内容为空
        return;
    }
    
    if (![MFMessageComposeViewController canSendText]) {
        //设备不支持发送短信
        return;
    }
    
    MFMessageComposeViewController *messageVC = [MFMessageComposeViewController new];
    messageVC.recipients = [self numbersFrom:contacts];
    messageVC.body = content;
    messageVC.messageComposeDelegate = self;
    [self showViewController:messageVC sender:self];
    
}

- (NSArray *)numbersFrom:(NSArray *)contacts {
    if (!contacts) {
        return nil;
    }
    NSMutableArray *mutableArray = [NSMutableArray new];
    for (ESAContact *contact in contacts) {
        [mutableArray addObject:contact.number];
    }
    return [NSArray arrayWithArray:mutableArray];
}

@end
