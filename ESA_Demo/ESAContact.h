//
//  ESAContact.h
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESAContact : NSObject<NSCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *number;

+ (instancetype)contactWithName:(NSString *)name number:(NSString *)number;

@end
