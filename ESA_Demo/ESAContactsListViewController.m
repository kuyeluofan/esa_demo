//
//  ESAContactsListViewController.m
//  ESA_Demo
//
//  Created by kuyeluofan on 9/15/16.
//  Copyright © 2016 kuyeluofan. All rights reserved.
//

#import "ESAContactsListViewController.h"
#import <ContactsUI/ContactsUI.h>
#import "Masonry.h"
#import "ESAContact.h"
#import "ESAContactHelper.h"

static NSString *const kUITableViewCellReuseID = @"UITableViewCellReuseID";

@interface ESAContactsListViewController ()<CNContactPickerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *contactsArray;

@end


@implementation ESAContactsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    self.contactsArray = [ESAContactHelper readContacts];//进入VC时读取已存的联系人数据
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [ESAContactHelper saveContacts:self.contactsArray];//离开VC时存入联系人数据
}

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"Contacts";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                           target:self
                                                                                           action:@selector(onClickRightBarButton)];
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.backgroundColor = [UIColor whiteColor];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView;
    });
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - Event Response
- (void)onClickRightBarButton {
    CNContactPickerViewController *pickerVC = [CNContactPickerViewController new];
    pickerVC.delegate = self;
    //存在电话号码的联系人才能被选中
    pickerVC.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"phoneNumbers.@count > 0"];
    [self presentViewController:pickerVC animated:YES completion:nil];
}


#pragma mark - Contact Picker Delegate
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContacts:(NSArray<CNContact *> *)contacts {
    //得到选中的联系人原始数据，转换成ESAContact并写到数组
    for (CNContact *CNContact in contacts) {
        NSString *contactName = [NSString stringWithFormat:@"%@ %@", CNContact.givenName, CNContact.familyName];
        NSString *contactNumber = [[CNContact.phoneNumbers[0] value] stringValue];
        ESAContact *contact = [ESAContact contactWithName:contactName number:contactNumber];
        if (![self.contactsArray containsObject:contact]) {
            [self.contactsArray addObject:contact];
        }
    }
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate & DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contactsArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUITableViewCellReuseID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kUITableViewCellReuseID];
        cell.textLabel.font = [UIFont systemFontOfSize:20];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:18];
    }
    ESAContact *contact = self.contactsArray[indexPath.row];
    cell.textLabel.text = contact.name;
    cell.detailTextLabel.text = contact.number;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //处理滑动删除事件
        [self.contactsArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}



@end
